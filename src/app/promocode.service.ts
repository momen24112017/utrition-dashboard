import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PromocodeService {

  url = "https://uttrition-test.azurewebsites.net/public/api"
  constructor(private http:HttpClient) { }
  getPromocodes(): Observable<any> {
    return this.http.get(`https://uttrition-test.azurewebsites.net/public/api/readPromocode`)
  }
  deletePromoCode(id){
    return this.http.get(`${this.url}/deletePromocode/${id}`)
  }
  getPromocodeById(id){
    return this.http.get(`${this.url}/viewPromocode/${id}`);
  }
  updatePromocode(obj,id){
    return this.http.post(`${this.url}/updatePromocode/${id}`,obj)
  }
  addPromoCode(obj){
    return this.http.post(`${this.url}/createPromocode`,obj)
  }
}
