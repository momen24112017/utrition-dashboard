import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ECommerceComponent } from './e-commerce.component';
import { CustomersComponent } from './customers/customers.component';
import { PlansComponent } from './plans/plans.component';
import { ProductEditComponent } from './plans/product-edit/product-edit.component';
import { PromoCodeComponent } from './promo-code/promo-code.component';
import { UseradminComponent } from './useradmin/useradmin.component';
import { EditPlanComponent } from './plans/edit-plan/edit-plan.component'
const routes: Routes = [
  {
    path: '',
    component: ECommerceComponent,
    children: [
      {
        path: 'clients',
        component: CustomersComponent,
      },
      {
        path: 'plans',
        component: PlansComponent,
      },
      {
        path: 'plan/add',
        component: ProductEditComponent
      },
      {
        path: 'plan/edit',
        component: ProductEditComponent
      },
      {
        path: 'plan/edit/:id',
        component: ProductEditComponent
      },
      {
        path: 'promo-code',
        component: PromoCodeComponent
      },
      {
        path: 'user-admin',
        component: UseradminComponent
      },
      {
        path: 'plan/edit-plan/:id',
        component: EditPlanComponent
      },
      { path: '', redirectTo: 'customers', pathMatch: 'full' },
      { path: '**', redirectTo: 'customers', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ECommerceRoutingModule { }
