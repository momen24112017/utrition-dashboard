import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of, Subscription } from 'rxjs';
import { catchError, delay, finalize, tap } from 'rxjs/operators';
import { PlansService } from 'src/app/plans.service';
import { PromocodeService } from 'src/app/promocode.service';
import { ProductsService } from '../../../_services';

@Component({
  selector: 'app-delete-product-modal',
  templateUrl: './delete-product-modal.component.html',
  styleUrls: ['./delete-product-modal.component.scss']
})
export class DeleteProductModalComponent implements OnInit, OnDestroy {

  @Input() id: number;
  @Input() type: string;
  isLoading = false;
  subscriptions: Subscription[] = [];

  constructor(
    private promocodeService: PromocodeService,
    private productsService: ProductsService,
    public modal: NgbActiveModal,
    private planservice: PlansService
  ) { }

  ngOnInit(): void {
  }

  deleteProduct() {
    this.isLoading = true;
    if (this.type == 'promo') {
      this.promocodeService.deletePromoCode(this.id).subscribe(res => {
        console.log(res);
        this.modal.close();
      }, err => {
        console.log(err);
        this.modal.close();
      })
    } else if (this.type == 'plan') {
      this.planservice.deletePlan(this.id).subscribe(res => {
        console.log(res);
        this.modal.close();

      }, err => {
        console.log(err);
        this.modal.close();

      })
    }
    // const sb = this.productsService.delete(this.id).pipe(
    //   // delay(1000), // Remove it from your code (just for showing loading)
    //   tap(() => this.modal.close()),
    //   catchError((err) => {
    //     this.modal.dismiss(err);
    //     return of(undefined);
    //   }),
    //   finalize(() => {
    //     this.isLoading = false;
    //   })
    // ).subscribe();
    // this.subscriptions.push(sb);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }
}
