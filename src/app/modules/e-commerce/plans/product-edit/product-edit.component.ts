import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { PromocodeService } from 'src/app/promocode.service';
import { Product } from '../../_models/product.model';
import { ProductsService } from '../../_services';

const EMPTY_PRODUCT: Product = {
  id: undefined,
  model: '',
  manufacture: 'Pontiac',
  modelYear: 2020,
  mileage: 0,
  description: '',
  color: 'Red',
  price: 0,
  condition: 1,
  status: 1,
  VINCode: '',
};

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit, OnDestroy {
  id: number;
  promocodeObj = {
    id: '',
    promocode: '',
    value: '',
    valid_date: ''

  };
  previous: Product;
  formGroup: FormGroup;
  isLoading$: Observable<boolean>;
  errorMessage = '';
  tabs = {
    BASIC_TAB: 0,
    REMARKS_TAB: 1,
    SPECIFICATIONS_TAB: 2
  };
  activeTabId = this.tabs.BASIC_TAB; // 0 => Basic info | 1 => Remarks | 2 => Specifications
  private subscriptions: Subscription[] = [];
  page_type;
  @ViewChild('savebutton') savebutton: ElementRef;
  constructor(
    private fb: FormBuilder,
    private productsService: ProductsService,
    private router: Router,
    private route: ActivatedRoute,
    private promocodeservice: PromocodeService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.isLoading$ = this.productsService.isLoading$;
    if (this.route.snapshot.paramMap.get('id')) {
      this.loadProduct();
      console.log('this is edit page');
      this.page_type = 'edit';
    } else {
      console.log('this is add page');
      this.page_type = 'add';
      this.formGroup = this.fb.group({
        promocode: [this.promocodeObj.promocode, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])],
        valid_date: [this.promocodeObj.valid_date],
        value: [this.promocodeObj.value, Validators.compose([Validators.required,])]
      });
    }
  }
  loadP
  loadProduct() {
    const sb = this.route.paramMap.pipe(
      switchMap(params => {
        // get id from URL
        this.id = Number(params.get('id'));
        if (this.id || this.id > 0) {
          return this.productsService.getItemById(this.id);
        }
        return of(EMPTY_PRODUCT);
      }),
      catchError((errorMessage) => {
        this.errorMessage = errorMessage;
        return of(undefined);
      }),
    ).subscribe((res: Product) => {
      // if (!res) {
      //   this.router.navigate(['/products'], { relativeTo: this.route });
      // }

      this.promocodeservice.getPromocodeById(this.id).subscribe(res => {
        let data;
        data = res
        this.promocodeObj = data.data;
        console.log(this.promocodeObj);
        this.loadForm();
      });
      // this.previous = Object.assign({}, res);
    });
    this.subscriptions.push(sb);
  }

  loadForm() {
    if (!this.promocodeObj) {
      return;
    }
    this.formGroup = this.fb.group({
      promocode: [this.promocodeObj.promocode, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(100)])],
      valid_date: [this.promocodeObj.valid_date],
      value: [this.promocodeObj.value, Validators.compose([Validators.required,])]
    });
  }

  reset() {
    if (!this.previous) {
      return;
    }

    // this.promocodeObj = Object.assign({}, this.previous);
    this.formGroup.reset();
  }

  save() {
    this.savebutton.nativeElement.disabled = true;


    if (this.page_type == 'edit') {
      console.log(this.formGroup);
      console.log(this.promocodeObj);
      this.promocodeservice.updatePromocode(this.formGroup.value, this.id).subscribe(res => {
        console.log(res);
        this.toastr.success('Promocode edited successfully');
        this.savebutton.nativeElement.disabled = false;

      }, err => {
        this.savebutton.nativeElement.disabled = false;

        console.log(err);
        this.toastr.error('Something went wrong');
      })
    } else if (this.page_type == 'add') {
      this.promocodeservice.addPromoCode(this.formGroup.value).subscribe(res => {
        console.log(res);
        this.toastr.success('Promocode added successfully');
        this.savebutton.nativeElement.disabled = false;
        this.formGroup.reset();
      }, err => {
        this.savebutton.nativeElement.disabled = false;

        console.log(err);
        this.toastr.error('Something went wrong');
      })
    }

    // this.formGroup.markAllAsTouched();
    // if (!this.formGroup.valid) {
    //   return;
    // }

    // const formValues = this.formGroup.value;
    // this.promocodeObj = Object.assign(this.promocodeObj, formValues);
    // if (this.id) {
    //   this.edit();
    // } else {
    //   this.create();
    // }
  }

  edit() {
    const sbUpdate = this.productsService.update(this.promocodeObj).pipe(
      tap(() => this.router.navigate(['/ecommerce/products'])),
      catchError((errorMessage) => {
        console.error('UPDATE ERROR', errorMessage);
        return of(this.promocodeObj);
      })
    ).subscribe(res => this.promocodeObj = res);
    this.subscriptions.push(sbUpdate);
  }

  create() {
    const sbCreate = this.productsService.create(this.promocodeObj).pipe(
      tap(() => this.router.navigate(['/ecommerce/products'])),
      catchError((errorMessage) => {
        console.error('UPDATE ERROR', errorMessage);
        return of(this.promocodeObj);
      })
    ).subscribe(res => {
      // this.promocodeObj = res as Product
    }



    );
    this.subscriptions.push(sbCreate);
  }

  changeTab(tabId: number) {
    this.activeTabId = tabId;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  // helpers for View
  isControlValid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.valid && (control.dirty || control.touched);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  controlHasError(validation: string, controlName: string) {
    const control = this.formGroup.controls[controlName];
    return control.hasError(validation) && (control.dirty || control.touched);
  }

  isControlTouched(controlName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control.dirty || control.touched;
  }
  back() {
    this.router.navigate(['/ecommerce/promo-code'])
  }
}
