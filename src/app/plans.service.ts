import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlansService {
  url = "https://uttrition-test.azurewebsites.net/public/api"
  constructor(private http: HttpClient) { }
  getPlans() {
    return this.http.get(`${this.url}/readPlan`)
  }
  createplan(Obj) {
    return this.http.post(`${this.url}/createPlan`, Obj);
  }

  deletePlan(id) {
    return this.http.get(`${this.url}/deletePlan/${id}`);
  }

  updatePlan(Obj, id) {
    return this.http.post(`${this.url}/updatePlan/${id}`, Obj)
  }

  getPlanById(id) {
    return this.http.get(`${this.url}/viewPlan/${id}`)
  }


}
